import * as actionTypes from '../actions/actionTypes'


const initialState = {
    loading: false,
    error: null,
    users: []
}



const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USER_START:
            return start(state, action)
        case actionTypes.FETCH_USER_SUCCESS:
            return success(state, action)
        case actionTypes.FETCH_USER_FAIL:
            return fail(state, action)
        default:
            return state
    }
}

const start = (state, action) => {
    return {
        ...state,
        loading: true,
        error: null
    }
}
const success = (state, action) => {
    return {
        ...state,
        loading: false,
        error: null,
        users: action.users
    }
}
const fail = (state, action) => {
    return {
        ...state,
        loading: false,
        error: action.error
    }
}

export default reducer