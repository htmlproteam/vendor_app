import * as actionTypes from './actionTypes'
import Axios from 'axios';
const API_URL = process.env.REACT_APP_API_ENDPOINT;



export const fetchUserStart = () => {
    return {
        type: actionTypes.FETCH_USER_START
    }
}
export const fetchUserSuccess = (users) => {
    return {
        type: actionTypes.FETCH_USER_SUCCESS,
        users: users
    }
}
export const fetchUserFail = (error) => {
    return {
        type: actionTypes.FETCH_USER_FAIL,
        error: error
    }
}

export const fetchUsers = () => {

    return dispatch => {
        dispatch(fetchUserStart())
        Axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');
        const url = API_URL + 'users'
        Axios.get(url)
            .then(res => {
                let fetchUsers = [];
                if (res.statusText === 'OK') {
                    for (let user of res.data.result.data) {
                        fetchUsers.push(user);
                    }
                }
                dispatch(fetchUserSuccess(fetchUsers));
            })
            .catch(err => {
                dispatch(fetchUserFail(err));
            })
    }
}

// API's
// GET:  users
// GET:  users/single/{id}
// POST:  users/update
// POST:  user/change-status