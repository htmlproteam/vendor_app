import React from 'react'

const Footer = () => {
    return (
        <footer>
            <div className="container-fluid ">
                <div className="row">
                    <div className="col-md-2 d-none d-md-block"></div>
                    <div className="col-md-8 ml-sm-auto col-lg-10 text-muted">B2B @ {new Date().getFullYear()}</div>

                </div>
            </div>
        </footer>
    )
}

export default Footer
