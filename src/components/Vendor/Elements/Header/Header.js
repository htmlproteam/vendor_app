import React from 'react'
import { Link } from 'react-router-dom'

{/* <button onClick={props.toggleSideBarShow}</button> */ }
const Header = (props) => {
    return (

        <header>
            <nav className="navbar navbar-brown-1 sticky-top bg-brown-1 flex-md-nowrap">
                <a className="navbar-brand col-sm-3 col-md-2 mr-0 bg-brown-1" href="#">Company name</a>

                <ul className="navbar-nav px-3">
                    <li className="nav-item text-nowrap">
                        <a className="nav-link" href="#">Sign out</a>
                    </li>
                </ul>
            </nav>

        </header >
    )
}

export default Header
