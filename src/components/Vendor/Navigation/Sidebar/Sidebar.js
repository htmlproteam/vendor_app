import React from 'react'
import { NavLink } from 'react-router-dom'
import Octicon, { Package, Database, Home, ThreeBars, Info } from '@primer/octicons-react'

const Sidebar = (props) => {

    // props.sideBarShow

    return (
        <nav className="col-md-2 d-none d-md-block bg-brown-2   sidebar">
            <div className="sidebar-sticky">
                <ul className="nav flex-column ">
                    <li className="nav-item ">
                        <NavLink className="nav-link" exact to={'/store/'}>
                            <Octicon icon={Home} verticalAlign='middle' /> Store
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to={'/store/company-information'}>
                            <Octicon icon={Info} verticalAlign='middle' />
                            Company Information
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to={'/store/manage-company-information'}>
                            <Octicon icon={Database} verticalAlign='middle' />
                            Manage Company Information
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to={'/store/manufacturing-capabilites'}>
                            <Octicon icon={ThreeBars} verticalAlign='middle' />
                            Manufacturing Capabilites
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to={'/store/exports'}>
                            <Octicon icon={Package} verticalAlign='middle' />
                            Exports
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default Sidebar
