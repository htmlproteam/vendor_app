import React, { Fragment } from 'react'
import RightCol from './RightCol'

const CompanyInformation = () => {
    return (
        <Fragment>
            <h2 className="text-brown-2">Company Information</h2>
            <div className="row dashboard-main">
                <div className="col-8">

                    <form>
                        <div className="form-group">
                            <label >Compay Logo</label>
                        </div>

                        <div className="form-group">
                            <div className="custom-file col-6">
                                <input type="file" className="custom-file-input" id="companyLogo" />
                                <label className="custom-file-label" htmlFor="companyLogo">Choose file...</label>
                            </div>
                            <small className="form-text text-muted">
                                200KB max. JPEG, PNG format only. Suggested photo width and height: 100*100px.
                            </small>
                        </div>


                        <div className="form-group">
                            <label>Company Description:</label>
                            <textarea className="form-control" rows='4'></textarea>
                        </div>

                        <div className="form-group">
                            <label >Compay Photos</label>
                        </div>
                        <div className="form-group">
                            <div className="custom-file col-6">
                                <input type="file" name="companyPhotos[]" className="custom-file-input" id="companyPhotos" multiple />
                                <label className="custom-file-label" htmlFor="companyPhotos">Choose file...</label>
                            </div>
                            <small className="form-text text-muted">
                                200KB max. JPEG or PNG format only. Suggested photo width and height for the new version Minisite: 1200*675px.
                                </small>
                        </div>

                        <div className="form-group ">
                            <small className="form-text text-muted">
                                By clicking 'Submit', all the information in the Manage Company Information section will be submitted. Once submitted, no further editing is allowed within one working day.
                            </small>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>

                </div>



                <div className="col-3 ml-auto">
                    <RightCol />
                </div>
            </div>
        </Fragment>
    )
}

export default CompanyInformation
