import React, { Fragment } from 'react'
import RightCol from './RightCol'

const ManufacturingCapability = () => {
    return (
        <Fragment>
            <h2 className="text-brown-2">Manufacturing Capability</h2>
            <div className="row dashboard-main">
                <div className="col-8">
                    <form>
                        <div className=" row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Whether to show production process:
                            </label>
                            <div className="col-sm-10 form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="productionProcess" id="productionProcessNo" value="no" />
                                    <label class="form-check-label" htmlFor="productionProcessNo">No</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="productionProcess" id="productionProcessYes" value="yes" />
                                    <label class="form-check-label" htmlFor="productionProcessYes">Yes</label>
                                </div>
                                <div className="customer-case-form border p-3">
                                    <div className="form-group ">
                                        <label htmlFor="processName">Process name:</label>
                                        <input type="text" className="form-control" id="processName" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="processPictures">Process Pictures:</label>
                                        <div className="custom-file">
                                            <input type="file" name="processPictures[]" className="custom-file-input" id="processPictures" />
                                            <label className="custom-file-label" htmlFor="processPictures">Choose files...</label>
                                        </div>
                                        <small className="form-text text-muted">Maximum 2M support JPG and PNG format</small>
                                    </div>
                                    <div className="form-group">
                                        <label>Process describe:</label>
                                        <textarea className="form-control" rows='4' maxLength='256'></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className=" row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Whether to show production equipment:
                            </label>
                            <div className="col-sm-10 form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="productionEquipment" id="productionEquipmentNo" value="no" />
                                    <label class="form-check-label" htmlFor="productionEquipmentNo">No</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="productionEquipment" id="productionEquipmentYes" value="yes" />
                                    <label class="form-check-label" htmlFor="productionEquipmentYes">Yes</label>
                                </div>
                                <div className="customer-case-form border p-3">
                                    <div className="form-group ">
                                        <label htmlFor="equipmentName">Equipment Name:</label>
                                        <input type="text" className="form-control" id="equipmentName" />
                                    </div>
                                    <div className="form-group ">
                                        <label htmlFor="equipmentModel">Equipment Model:</label>
                                        <input type="text" className="form-control" id="equipmentModel" />
                                    </div>
                                    <div className="form-group ">
                                        <label htmlFor="equipmentQuantity">Equipment quantity:</label>
                                        <input type="text" className="form-control" id="equipmentQuantity" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className=" row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Whether to show Production Line:
                            </label>
                            <div className="col-sm-10 form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="productionEquipment" id="productionEquipmentNo" value="no" />
                                    <label class="form-check-label" htmlFor="productionEquipmentNo">No</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="productionEquipment" id="productionEquipmentYes" value="yes" />
                                    <label class="form-check-label" htmlFor="productionEquipmentYes">Yes</label>
                                </div>
                                <div className="customer-case-form border p-3">
                                    <div className="form-group ">
                                        <label htmlFor="productionLineName">Production Line name:</label>
                                        <input type="text" className="form-control" id="productionLineName" />
                                    </div>
                                    <div className="form-group ">
                                        <label htmlFor="supervisorNumber">Supervisor Number:</label>
                                        <input type="text" className="form-control" id="supervisorNumber" />
                                    </div>
                                    <div className="form-group ">
                                        <label htmlFor="numberOfOperators">Number of Operators:</label>
                                        <input type="text" className="form-control" id="numberOfOperators" />
                                    </div>
                                    <div className="form-group ">
                                        <label htmlFor="qcqaNumber">QC/QA Number:</label>
                                        <input type="text" className="form-control" id="qcqaNumber" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">

                            <label htmlFor="factoryLocation" className="col-sm-2 col-form-label">
                                Factory Location:
                        </label>
                            <div className="col-sm-10 form-group">
                                <input type="text" className="form-control" id="factoryLocation" />
                            </div>

                            <label htmlFor="factoryLocation" className="col-sm-2 col-form-label">
                                Factory Size:
                        </label>
                            <div className="col-sm-10 form-group">
                                <select id="factoryLocation" className="form-control">
                                    <option defaultValue>Select</option>
                                    <option>...</option>
                                </select>
                            </div>
                            <label htmlFor="factoryLocation" className="col-sm-2 col-form-label">
                                Contract Manufacturing:
                        </label>
                            <div className="col-sm-10 form-group d-flex flex-column flex-wrap">
                                <div className="form-check  align-self-start">
                                    <input className="form-check-input" type="checkbox" value="" id="cbOEMServiceOffered" />
                                    <label className="form-check-label" for="cbOEMServiceOffered">OEM Service Offered</label>
                                </div>
                                <div className="form-check  align-self-start">
                                    <input className="form-check-input" type="checkbox" value="" id="cbDesignServiceOffered" />
                                    <label className="form-check-label" for="cbDesignServiceOffered">Design Service Offered</label>
                                </div>
                                <div className="form-check  align-self-start">
                                    <input className="form-check-input" type="checkbox" value="" id="cbBuyerLabelOffered" />
                                    <label className="form-check-label" for="cbBuyerLabelOffered">Buyer Label Offered</label>
                                </div>
                            </div>
                            <label htmlFor="numOfQCStaff" className="col-sm-2 col-form-label">
                                No. of QC Staff:
                        </label>
                            <div className="col-sm-10 form-group">
                                <select id="numOfQCStaff" className="form-control">
                                    <option defaultValue>Select</option>
                                    <option>...</option>
                                </select>
                            </div>
                            <label htmlFor="numOfRDStaff" className="col-sm-2 col-form-label">
                                No. of R &amp; D Staff:
                        </label>
                            <div className="col-sm-10 form-group">
                                <select id="numOfRDStaff" className="form-control">
                                    <option defaultValue>Select</option>
                                    <option>...</option>
                                </select>
                            </div>
                            <label htmlFor="numOfProductionLines" className="col-sm-2 col-form-label">
                                No. of Production Lines:
                        </label>
                            <div className="col-sm-10 form-group">
                                <select id="numOfProductionLines" className="form-control">
                                    <option defaultValue>Select</option>
                                    <option>...</option>
                                </select>
                            </div>
                            <label htmlFor="annualOutputValue" className="col-sm-2 col-form-label">
                                Annual Output Value:
                        </label>
                            <div className="col-sm-10 form-group">
                                <select id="annualOutputValue" className="form-control">
                                    <option defaultValue>Select</option>
                                    <option>...</option>
                                </select>
                            </div>

                            <label className="col-sm-2 col-form-label">
                                Add information about your annual production capacity?:
                            </label>
                            <div className="col-sm-10 form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="annualProduction" id="annualProductionNo" value="no" />
                                    <label class="form-check-label" htmlFor="annualProductionNo">No</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="annualProduction" id="annualProductionYes" value="yes" />
                                    <label class="form-check-label" htmlFor="annualProductionYes">Yes</label>
                                </div>
                                <div className="customer-case-form border p-3">
                                    <div className="form-group ">
                                        <label htmlFor="productName">Product Name:</label>
                                        <input type="text" className="form-control" id="productName" />
                                    </div>
                                    <div className="form-group ">
                                        <label htmlFor="unitProduced">Units Produced (Previous Year):</label>
                                        <input type="text" className="form-control" id="unitProduced" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            <small className="text-muted">By clicking 'Submit', all the information in the Manage Company Information section will be submitted. Once submitted, no further editing is allowed within one working day.</small>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div className="col-3 ml-auto">
                    <RightCol />
                </div>
            </div>
        </Fragment>
    )
}

export default ManufacturingCapability
