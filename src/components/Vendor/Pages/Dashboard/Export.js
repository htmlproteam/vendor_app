import React, { Fragment } from 'react'
import RightCol from './RightCol'

const Export = () => {
    return (
        <Fragment>
            <h2 className="text-brown-2">Exports</h2>
            <div className="row dashboard-main">
                <div className="col-8">
                    <form>

                        <div className="form-group row">
                            <label for="totalAnnualRevenue" className="col-sm-2 col-form-label">Total Annual Revenue:</label>
                            <div className="col-sm-10">
                                <select id="totalAnnualRevenue" className="form-control">
                                    <option defaultValue>Please Select</option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label htmlFor="exportPercentage" className="col-sm-2 col-form-label">Export Percentage:</label>
                            <div className="col-sm-10">
                                <select id="exportPercentage" className="form-control">
                                    <option defaultValue>Please Select</option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Main Markets and Distribution: 0.00%
                            </label>

                            <div className="col-sm-10 d-flex flex-row flex-wrap">
                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % North America
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % South America
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % Southeast Asia
                                </div>

                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % Africa
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % Oceania
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % Mid East
                                </div>


                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % Eastern Asia
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % Western Europe
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % Central America
                                </div>


                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % Northern Europe
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % Southern Europe
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % South Asia
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-1 align-self-center p-0 mb-1">
                                    <input type="number" className="form-control input-main-market" placeholder="0" />
                                </div>
                                <div className="col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    % Domestic Market
                                </div>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label htmlFor="companyStartingYear" className="col-sm-2 col-form-label">
                                Year when your company started exporting:
                            </label>
                            <div className="col-sm-10">
                                <select id="companyStartingYear" className="form-control">
                                    <option defaultValue>Please Select</option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Whether add customer case:
                            </label>
                            <div className="col-sm-10">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="customerCare" id="customerCareNo" value="no" />
                                    <label class="form-check-label" htmlFor="customerCareNo">No</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="customerCare" id="customerCareYes" value="yes" />
                                    <label class="form-check-label" htmlFor="customerCareYes">Yes</label>
                                </div>

                                <div className="customer-case-form border p-3">
                                    <div className="form-group ">
                                        <label htmlFor="customerName">Project/Customer Name:</label>
                                        <input type="text" className="form-control" id="customerName" />
                                    </div>
                                    <div className="form-group ">
                                        <label htmlFor="">Customer Country Region:</label>
                                        <select id="" className="form-control">
                                            <option defaultValue>Select</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div className="form-group ">
                                        <label htmlFor="productSupplyToCustomer">Products You supply to Customer:</label>
                                        <input type="text" className="form-control" id="productSupplyToCustomer" />
                                    </div>
                                    <div className="form-group ">
                                        <label htmlFor="annualTurnover">Annual Turnover US$:</label>
                                        <input type="text" className="form-control" id="annualTurnover" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="cooperationPhotos">Cooperation photos:</label>
                                        <div className="custom-file">
                                            <input type="file" name="cooporationPhotos[]" className="custom-file-input" id="cooporationPhotos" />
                                            <label className="custom-file-label" htmlFor="cooporationPhotos">Choose file...</label>
                                        </div>
                                        <small className="form-text text-muted">
                                            Maximum 2M support JPG and PNG format
                                        </small>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="transactionDocuments">Transaction Documents:</label>
                                        <div className="custom-file">
                                            <input type="file" name="transactionDocuments[]" className="custom-file-input" id="transactionDocuments" />
                                            <label className="custom-file-label" htmlFor="transactionDocuments">Choose file...</label>
                                        </div>
                                        <small className="form-text text-muted">
                                            Include commercial invoices, packing lists, etc., 2M Max, JPEG or PNG format only.
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label htmlFor="totalEmployeeTradeDep" className="col-sm-2 col-form-label">No. of Employees in Trade Department:</label>
                            <div className="col-sm-10">
                                <select id="totalEmployeeTradeDep" className="form-control">
                                    <option defaultValue>Please Select</option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>



                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Nearest Port:
                            </label>
                            <div className="col-sm-10">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <input type="text" className="form-control" id="nearestPort1" placeholder="" />
                                    </div>
                                    <div className="col-sm-4">
                                        <input type="text" className="form-control" id="nearestPort1" placeholder="" />
                                    </div>
                                    <div className="col-sm-4">
                                        <input type="text" className="form-control" id="nearestPort1" placeholder="" />
                                    </div>
                                </div>
                                <small className="text-muted">One port name per box</small>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Average Lead Time:
                            </label>
                            <div className="col-sm-10">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <input type="text" className="form-control" id="nearestPort1" placeholder="" />
                                    </div>
                                    <div className="col-sm-4 align-self-center">
                                        Days(s)
                                    </div>
                                </div>
                                <small className="text-muted">Please enter the average production time. Numbers only.</small>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Does your company have an overseas office?
                            </label>
                            <div className="col-sm-10">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="customerCare" id="customerCareNo" value="no" />
                                    <label class="form-check-label" htmlFor="customerCareNo">No</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="customerCare" id="customerCareYes" value="yes" />
                                    <label class="form-check-label" htmlFor="customerCareYes">Yes</label>
                                </div>
                            </div>
                        </div>


                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Accepted Delivery Terms:
                            </label>
                            <div className="col-sm-10 d-flex flex-row flex-wrap">

                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbFOB" />
                                    <label className="form-check-label" for="cbFOB">FOB</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbCFR" />
                                    <label className="form-check-label" for="cbCFR">CFR</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbCIF" />
                                    <label className="form-check-label" for="cbCIF">CIF</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbEXW" />
                                    <label className="form-check-label" for="cbEXW">EXW</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbFAS" />
                                    <label className="form-check-label" for="cbFAS">FAS</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbCIP" />
                                    <label className="form-check-label" for="cbCIP">CIP</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbFCA" />
                                    <label className="form-check-label" for="cbFCA">FCA</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbCPT" />
                                    <label className="form-check-label" for="cbCPT">CPT</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbDEQ" />
                                    <label className="form-check-label" for="cbDEQ">DEQ</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbDDP" />
                                    <label className="form-check-label" for="cbDDP">DDP</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbDDU" />
                                    <label className="form-check-label" for="cbDDU">DDU</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbExpressDelivery" />
                                    <label className="form-check-label" for="cbExpressDelivery">Express Delivery</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbDAF" />
                                    <label className="form-check-label" for="cbDAF">DAF</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbDES" />
                                    <label className="form-check-label" for="cbDES">DES</label>
                                </div>

                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Accepted Payment Currency:
                            </label>
                            <div className="col-sm-10 d-flex flex-row flex-wrap">
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbUSD" />
                                    <label className="form-check-label" for="cbUSD">USD</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbEUR" />
                                    <label className="form-check-label" for="cbEUR">EUR</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbJPY" />
                                    <label className="form-check-label" for="cbJPY">JPY</label>
                                </div>

                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbCAD" />
                                    <label className="form-check-label" for="cbCAD">CAD</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbAUD" />
                                    <label className="form-check-label" for="cbAUD">AUD</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbHKD" />
                                    <label className="form-check-label" for="cbHKD">HKD</label>
                                </div>

                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbGBP" />
                                    <label className="form-check-label" for="cbGBP">GBP</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbCNY" />
                                    <label className="form-check-label" for="cbCNY">CNY</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbCHF" />
                                    <label className="form-check-label" for="cbCHF">CHF</label>
                                </div>
                            </div>

                        </div>
                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Accepted Payment Type:
                            </label>
                            <div className="col-sm-10 d-flex flex-row flex-wrap">
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbTT" />
                                    <label className="form-check-label" for="cbTT">T/T</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbLC" />
                                    <label className="form-check-label" for="cbLC">L/C</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbDpda" />
                                    <label className="form-check-label" for="cbDpda">D/P D/A</label>
                                </div>

                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbMoneyGram" />
                                    <label className="form-check-label" for="cbMoneyGram">MoneyGram</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbCreditCard" />
                                    <label className="form-check-label" for="cbCreditCard">Credit Card</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbPaypal" />
                                    <label className="form-check-label" for="cbPaypal">PayPal</label>
                                </div>

                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbWesternUnion" />
                                    <label className="form-check-label" for="cbWesternUnion">Western Union</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbCash" />
                                    <label className="form-check-label" for="cbCash">Cash</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbEscrow" />
                                    <label className="form-check-label" for="cbEscrow">Escrow</label>
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                Language Spoken:
                            </label>
                            <div className="col-sm-10 d-flex flex-row flex-wrap">
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbEnglish" />
                                    <label className="form-check-label" for="cbEnglish">English</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbChinese" />
                                    <label className="form-check-label" for="cbChinese">Chinese</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbspanish" />
                                    <label className="form-check-label" for="cbspanish">Spanish</label>
                                </div>

                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbJapanese" />
                                    <label className="form-check-label" for="cbJapanese">Japanese</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbPortuguese" />
                                    <label className="form-check-label" for="cbPortuguese">Portuguese</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbGerman" />
                                    <label className="form-check-label" for="cbGerman">German</label>
                                </div>

                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbArabic" />
                                    <label className="form-check-label" for="cbArabic">Arabic</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbFrench" />
                                    <label className="form-check-label" for="cbFrench">French</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbRussian" />
                                    <label className="form-check-label" for="cbRussian">Russian</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbKorean" />
                                    <label className="form-check-label" for="cbKorean">Korean</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbItalian" />
                                    <label className="form-check-label" for="cbItalian">Italian</label>
                                </div>
                                <div className="form-check col-sm-4 col-md-2 col-lg-3 align-self-center">
                                    <input className="form-check-input" type="checkbox" value="" id="cbHindi" />
                                    <label className="form-check-label" for="cbHindi">Hindi</label>
                                </div>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label htmlFor="" className="col-sm-2 col-form-label">
                                ...
                            </label>
                            <div className="col-sm-10">
                                ...
                            </div>
                        </div>

                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div className="col-3 ml-auto">
                    <RightCol />
                </div>
            </div>
        </Fragment>
    )
}

export default Export
