import React, { Fragment } from 'react'
import RightCol from './RightCol'

const ManageCompanyInformation = () => {
    return (
        <Fragment>
            <h2 className="text-brown-2">Manage Company Information</h2>
            <div className="row dashboard-main">
                <div className="col-8">
                    <form>

                        <div className="form-group ">
                            <label htmlFor="companyName">Company Name:</label>
                            <input type="text" className="form-control" id="companyName" placeholder="Company Name" />
                        </div>
                        <div className="form-group ">
                            <label htmlFor="locationRegistration">Location of Registration:</label>
                            <select id="locationRegistration" className="form-control">
                                <option defaultValue>Country / Region</option>
                                <option>...</option>
                                <option>....</option>
                            </select>
                        </div>


                        <div className="form-group text-md font-weight-bold mt-4">Company Operational Address:</div>

                        <div className="form-group">
                            <label htmlFor="street">Street</label>
                            <input type="text" className="form-control" id="street" placeholder="Street" />
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label htmlFor="city">City</label>
                                <input type="text" className="form-control" id="city" placeholder="City" />
                            </div>
                            <div className="form-group col-md-4">
                                <label htmlFor="state">State</label>
                                <select id="state" className="form-control">
                                    <option defaultValue>Choose...</option>
                                    <option>...</option>
                                </select>
                            </div>
                            <div className="form-group col-md-2">
                                <label htmlFor="zip">Zip</label>
                                <input type="text" className="form-control" id="zip" placeholder="Zip code" />
                            </div>
                        </div>


                        <div className="form-group text-md font-weight-bold mt-4"></div>

                        <div className="form-group ">
                            <label htmlFor="componyRegistration">Year Company Registered:</label>
                            <select id="componyRegistration" className="form-control">
                                <option defaultValue>Select</option>
                                <option>...</option>
                            </select>
                        </div>

                        <div className="form-group ">
                            <label htmlFor="totalEmployees">Total No. Employees:</label>
                            <select id="totalEmployees" className="form-control">
                                <option defaultValue>Select</option>
                                <option>...</option>
                            </select>
                        </div>


                        <div className="form-group">
                            <label htmlFor="webUrl">Company Website Url:</label>
                            <input type="text" className="form-control" id="webUrl" placeholder="http://" />
                        </div>

                        <div className="form-group">
                            <label htmlFor="legalOwner">Legal Owner:</label>
                            <input type="text" className="form-control" id="legalOwner" placeholder="Legal Owner" />
                        </div>

                        <div className="form-group ">
                            <label htmlFor="officeSize">Office Size:</label>
                            <select id="officeSize" className="form-control">
                                <option defaultValue>Select</option>
                                <option>...</option>
                            </select>
                        </div>

                        <div className="form-group">
                            <label>Company Advantages:</label>
                            <textarea className="form-control" rows='4' maxLength='256'></textarea>
                            <small className="text-muted">Please briefly describe your company’s advantages. E.g. “We have twenty years experience of product design.”</small>
                        </div>

                        <div className="form-group">
                            <small className="text-muted">By clicking 'Submit', all the information in the Manage Company Information section will be submitted. Once submitted, no further editing is allowed within one working day.</small>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div className="col-3 ml-auto">
                    <RightCol />
                </div>
            </div>
        </Fragment>
    )
}

export default ManageCompanyInformation
