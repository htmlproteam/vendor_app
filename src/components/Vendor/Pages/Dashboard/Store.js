import React, { Fragment } from 'react'

const Store = () => {
    return (
        <Fragment>
            <h2 className="text-brown-2">Welcome to your Store </h2>
            <div className="row dashboard-main">
                <div className="col-8">
                    Welcome to Vendor Dashboard
                </div>
                <div className="col-3 ml-auto">
                    <ul className="list-group">

                        <li className="list-group-item disabled">Cras justo odio</li>
                        <li className="list-group-item">Dapibus ac facilisis in</li>
                        <li className="list-group-item">Morbi leo risus</li>
                        <li className="list-group-item">Porta ac consectetur ac</li>
                        <li className="list-group-item">Vestibulum at eros</li>
                    </ul>

                </div>
            </div>
        </Fragment>
    )
}

export default Store
