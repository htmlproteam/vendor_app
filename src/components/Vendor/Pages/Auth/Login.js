import React from 'react'

const Login = (props) => {
    return (
        <div className='container auth-form-section '>
            <h3 className='text-center'>Login </h3>
            <form method="post">
                <div className="form-group">
                    <input type="text" className="form-control" placeholder="Username" />
                </div>
                <div className="form-group">
                    <input type="password" className="form-control" placeholder="Password" />
                </div>
                <div className="form-group">
                    <button
                        className="btn btn-primary btn-block btn-lg"
                        onClick={props.handleDoAuth}
                    >Login</button>
                </div>
                <p className="text-center">
                    <hr /><span className='text-muted'>OR</span>
                </p>
                <div className="form-group">
                    <button
                        className="btn btn-brown-1 btn-block btn-lg"
                        onClick={props.handleClickRegister}
                    >Register</button>
                </div>
            </form>
            <p className="hint-text small"><a href="#">Forgot Your Password?</a></p>
        </div>
    )
}

export default Login
