import React from 'react'

const Register = (props) => {
    return (
        <div className='container auth-form-section '>
            <h3 className='text-center'>Register </h3>
            <form method="post">
                <div className="form-group">
                    <input type="email" className="form-control" placeholder="Email" />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" placeholder="First Name" />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" placeholder="Last Name" />
                </div>
                <div className="form-group">
                    <input type="password" className="form-control" placeholder="Password" />
                </div>
                <div className="form-group">
                    <input type="re-password" className="form-control" placeholder="Retype Password" />
                </div>
                <div className="form-group">
                    <button
                        className="btn btn-primary btn-block btn-lg"
                        onClick={props.handleDoAuth}
                    >Register</button>
                </div>
                <p className="text-center">
                    <hr /><span className='text-muted'>OR</span>
                </p>
                <div className="form-group">
                    <button
                        className="btn btn-brown-1 btn-block btn-lg"
                        onClick={props.handleClickLogin}
                    >Login</button>
                </div>
            </form>
        </div>
    )
}

export default Register
