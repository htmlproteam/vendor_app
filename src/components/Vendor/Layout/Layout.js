import React, { Fragment, useState } from 'react'

import Sidebar from '../Navigation/Sidebar/Sidebar';
import Header from '../Elements/Header/Header';
import Footer from '../Elements/Footer/Footer';

const Layout = (props) => {

    const [sideBarShow, setSideBarShow] = useState(true);

    const toggleSideBarShow = () => {
        setSideBarShow(!sideBarShow)
    }

    return (
        <Fragment>
            <Header toggleSideBarShow={toggleSideBarShow} />

            <div className="container-fluid">
                <div className="row">
                    <Sidebar sideBarShow={sideBarShow} />

                    <div className="col-md-8 ml-sm-auto col-lg-10 px-5 py-5 ">
                        {props.children}
                    </div>

                </div>
            </div>

            <Footer />

        </Fragment>
    )
}

export default Layout
