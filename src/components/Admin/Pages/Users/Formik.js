import React from 'react'
import { Formik, Form, ErrorMessage, Field } from "formik";
import * as Yup from 'yup';



const validationSchema = {
    firstName: Yup.string()
        .max(15, 'Must be 15 characters or less')
        .required('Required'),
    lastName: Yup.string()
        .max(20, 'Must be 20 characters or less')
        .required('Required'),
    email: Yup.string()
        .email('Invalid email address')
        .required('Required'),
}

const formConfig = { firstName: '', lastName: '', email: '' }

const Formik = () => {

    return (
        <Formik
            initialValues={formConfig}
            validationSchema={Yup.object(validationSchema)}
            onSubmit={(values, { setSubmitting }) => {
                console.log(values);
                // setSubmitting(false);
            }}
        >
            <Form className='form'>
                <label htmlFor="firstName">First Name</label>
                <Field className='error' name="firstName" type="text" />
                <ErrorMessage name="firstName" >
                    {msg => <em className="error invalid-feedback">{msg}</em>}
                </ErrorMessage>
                <label htmlFor="lastName">Last Name</label>
                <Field as="select" name="color">
                    <option value="red">Red</option>
                    <option value="green">Green</option>
                    <option value="blue">Blue</option>
                </Field>
                <ErrorMessage component="div" name="lastName" />
                <label htmlFor="email">Email Address</label>
                <Field name="email" type="email" />
                <ErrorMessage component="div" name="email" />
                <button type="submit">Submit</button>
            </Form>
        </Formik>
    );
}

export default Formik



// example usage
// const MyForm = () => (
//     <Formik
//       initialValues={{ email: '', username: '' }}
//       onSubmit={values => alert(JSON.stringify(values, null, 2))}
//     >
//       {({ errors, touched }) => (
//         <Form>
//           <Field validate={validate} name="email" type="email" />
//           {errors.email && touched.email ? <div>{errors.email}</div> : null}
//           <Field validate={validateAsync} name="username" />
//           {errors.username && touched.username ? (
//             <div>{errors.username}</div>
//           ) : null}
//           <button type="submit">Submit</button>
//         </Form>
//       )}
//     </Formik>
//   );