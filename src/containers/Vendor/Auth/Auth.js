import '../../../scss/Vendor/style.scss'

import React, { useState, useEffect, Fragment } from 'react'
import { Redirect, useHistory } from 'react-router-dom'
import Login from '../../../components/Vendor/Pages/Auth/Login'
import Register from '../../../components/Vendor/Pages/Auth/Register'

const Auth = (props) => {
    let history = useHistory();

    const [form, setForm] = useState({
        fname: '',
        lname: '',
        email: '',
        password: '',
        rePassword: ''
    });

    const handleOnChangeInput = (event) => {
        const { name, value } = event.target;
        setForm(prevState => {
            return {
                ...prevState,
                [name]: value
            }
        })
    }
    const handleClickLogin = () => {
        history.push("/login");
    }
    const handleClickRegister = () => {
        history.push("/register");
    }

    const handleDoAuth = () => {
        console.log('click auth...');
    }

    return (
        <div className='auth-main'>
            <header></header>
            <div className='d-flex justify-content-center align-items-center'>
                {
                    (props.formType === 'login') ?
                        <Login
                            handleDoAuth={handleDoAuth}
                            handleClickRegister={handleClickRegister}
                        />
                        :
                        <Register
                            handleDoAuth={handleDoAuth}
                            handleClickLogin={handleClickLogin}
                        />
                }
            </div>
            <footer></footer>
        </div >

    );
}

export default Auth
