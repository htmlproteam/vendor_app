import '../../../scss/Vendor/style.scss'
import React from 'react'
import Layout from '../../../components/Vendor/Layout/Layout'
import { Switch, Route, useRouteMatch } from 'react-router-dom'
import CompanyInformation from '../../../components/Vendor/Pages/Dashboard/CompanyInformation'
import ManageCompanyInformation from '../../../components/Vendor/Pages/Dashboard/ManageCompanyInformation'
import ManufacturingCapabilities from '../../../components/Vendor/Pages/Dashboard/ManufacturingCapability'
import Exports from '../../../components/Vendor/Pages/Dashboard/Export'
import Store from '../../../components/Vendor/Pages/Dashboard/Store'



const VendorDashboard = () => {
    let match = useRouteMatch();
    return (
        <Layout>
            <Switch>
                {/* <Route path={match.url + '/logout'} ></Route> */}
                <Route exact path={match.url + '/'} ><Store /></Route>
                <Route path={match.url + '/company-information'} ><CompanyInformation /></Route>
                <Route path={match.url + '/manage-company-information'} ><ManageCompanyInformation /></Route>
                <Route path={match.url + '/manufacturing-capabilites'} ><ManufacturingCapabilities /></Route>
                <Route path={match.url + '/exports'} ><Exports /></Route>
            </Switch>
        </Layout>
    )
}

export default VendorDashboard
